module simplenum_tb;

localparam MAX = 100_000;
localparam integer SIMPLEMAX = ($sqrt(MAX)); // maximum of simple value in range 
localparam integer S_ORDER = 66; // for 1M 170, 100k 66. ((SIMPLEMAX/$ln(SIMPLEMAX) + 1)); // maximum requirement of Eratosthenes sieve iterators (sieve order)


/* test clock and reset */
logic clk;
logic rst;

initial begin
    clk = 1'b0;
    forever begin
        #0.5ns; // 1 MHz
        clk = !clk;
    end
end


/* some message and startup */
initial  begin
  #1ns;
  rst <= 1'b1;
  #1ns;
  rst <= 1'b0;
  
  $display ("max value: %d, Simple await: %d, sieve order: %d", MAX, SIMPLEMAX, S_ORDER);
end

simplenum #(.MAX(MAX), .SIMPLEMAX(SIMPLEMAX), .S_ORDER(S_ORDER), .S_MAX(SIMPLEMAX)) dut (
  .clk_i(clk), // input clock
  .rst_i(rst),
	
  .out(),
  .sieveOver(),
  .isSimple()
);

endmodule;
	