vlib work


# compiling systemerilog files
vlog *.sv

# name for your testbench module
vsim -t 1ns -msgmode both -novopt simplenum_tb

#adding waveforms in hex view
#add wave -r -hex *

add wave -hex -radix binary -label clk -color blue /simplenum_tb/clk

add wave -hex -radix unsigned -label "number" -color blue	/simplenum_tb/dut/num

add wave -hex -radix unsigned -label "value" -color goldenrod	/simplenum_tb/dut/val
add wave -hex -radix unsigned -label "multipler" -color goldenrod	/simplenum_tb/dut/multipler

add wave -hex -radix binary -label isSimple -color green /simplenum_tb/dut/isSimple
add wave -hex -radix unsigned -label out -color green	/simplenum_tb/dut/out


#add wave -hex -radix unsigned -label simples -color goldenrod	/simplenum_tb/dut/simpleCnt
add wave -hex -radix binary -label error -color red /simplenum_tb/dut/sieveOver

#add wave -hex -radix unsigned -label "nextvalue" -color gold /simplenum_tb/dut/multipler
# running simulation
run 2us
