/*
\date 2015
\authors Andrew Strokov
\brief Lab 2, task B1: find all simple number to MAX
\copyright Metrotek FPGA lessons

*/

module simplenum
#(
  MAX = 100_000,
  SIMPLEMAX = 316,
  S_ORDER = 56,
  S_MAX = 316
)
(
	input clk_i, // input clock
	input rst_i,
	
	output logic [$clog2(MAX)-1:0] out, // one-shot key pressed
	output logic sieveOver,
	output logic isSimple
);

logic _isSimple;

logic [$clog2(MAX)-1:0] num;

logic [$clog2(MAX)-1:0] multipler [S_ORDER-1:0];
logic [$clog2(MAX)-1:0] val [S_ORDER-1:0];

logic [$clog2(MAX)-1:0] simpleCnt = 0;

always_comb begin
  _isSimple = 1'b1;
  for(int i=0;i < S_ORDER;i++) begin
    _isSimple &= (num > val[i]); // check that all sieve values has no update at this value
  end
end

always_ff @(posedge clk_i or posedge rst_i) begin // update sieve's counters to current number
  if(rst_i)
    for(int i=0;i < S_ORDER;i++)
      val[i] <= 0;
  else for(int i=0;i < S_ORDER;i++) begin
    if(val[i] + multipler[i] == num + 1) // next sieve value 
      val[i] <= val[i] + multipler[i];
    else if(_isSimple && simpleCnt < S_ORDER)// add simple num to out, and append it to sieve
      val[simpleCnt] <= num;
  end
end

always_ff @(posedge clk_i or posedge rst_i) begin // number counter
  if(rst_i)
    num <= 2;
  else
    num <= num + 1;
end

always_ff @(posedge clk_i or posedge rst_i) begin
  if(rst_i) begin
  
    for(int i=0;i < S_ORDER;i++)
      multipler[i] = 0;
    out <= 0;
    sieveOver <= 0;

  end else if(_isSimple) begin // add simple num to out, and append it to sieve
    out <= num;
    simpleCnt <= simpleCnt + 1;
    if(simpleCnt < S_ORDER)
      multipler[simpleCnt] <= num;
    else if(num < S_MAX || num > MAX) // if sieve value overrides before sqrt(S_ORDER) has calculated
	   sieveOver <= 1'b1;
  end else if(num > MAX)
    sieveOver <= 1'b1;
end


always_ff @(posedge clk_i or posedge rst_i)
  if(rst_i)
    isSimple <= 0;
  else
    isSimple <= _isSimple;

endmodule