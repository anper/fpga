// ============================================================================
// Copyright (c) 2013 by Terasic Technologies Inc.
// ============================================================================
//
// Permission:
//
//   Terasic grants permission to use and modify this code for use
//   in synthesis for all Terasic Development Boards and Altera Development 
//   Kits made by Terasic.  Other use of this code, including the selling 
//   ,duplication, or modification of any portion is strictly prohibited.
//
// Disclaimer:
//
//   This VHDL/Verilog or C/C++ source code is intended as a design reference
//   which illustrates how these types of functions can be implemented.
//   It is the user's responsibility to verify their design for
//   consistency and functionality through the use of formal
//   verification methods.  Terasic provides no warranty regarding the use 
//   or functionality of this code.
//
// ============================================================================
//           
//  Terasic Technologies Inc
//  9F., No.176, Sec.2, Gongdao 5th Rd, East Dist, Hsinchu City, 30070. Taiwan
//  
//  
//                     web: http://www.terasic.com/  
//                     email: support@terasic.com
//
// ============================================================================
//Date:  Mon Jun 17 20:35:29 2013
// ============================================================================

//`define ENABLE_HPS

module DE1_SoC_Default(


      ///////// CLOCK /////////
      input              CLOCK_50,
      
      ///////// KEY /////////
      input       [3:0]  KEY,

      ///////// LEDR /////////
      output      [9:0]  LEDR,

      ///////// SW /////////
      input       [9:0]  SW,

      ///////// HEX0 /////////
      output      [6:0]  HEX0,

      ///////// HEX1 /////////
      output      [6:0]  HEX1,

      ///////// HEX2 /////////
      output      [6:0]  HEX2,

      ///////// HEX3 /////////
      output      [6:0]  HEX3,

      ///////// HEX4 /////////
      output      [6:0]  HEX4,

      ///////// HEX5 /////////
      output      [6:0]  HEX5

);

logic [23:0] cnt;

always_ff @( posedge CLOCK_50 )
  cnt <= cnt + 1'd1;

assign LEDR = cnt[23:14];

/*
*/

// output signal
logic	[6:0]	h, h_0, h_1;	// hours
logic	[6:0]	m, m_0, m_1;	// minutes
logic	[6:0]	s, s_0, s_1;	// seconds

// input preset values
logic	[6:0]	h_pres;	// hours
logic	[6:0]	m_pres;	// minutes
logic	[6:0]	s_pres;	// seconds

logic s_rst;

logic key0_pressed_stb, key1_pressed_stb;

debounce #(.CLOCK_FREQ(1_000_000), .DEBOUNCE_TIME(10)) key0 (
	.clk_i(CLOCK_50),
	.key_i (KEY[0]),
	.key_pressed_stb_o (key0_pressed_stb),
	.key_released_stb_o (),
	.key_state_o ()
);
debounce #(.CLOCK_FREQ(1_000_000), .DEBOUNCE_TIME(10)) key1 (
	.clk_i(CLOCK_50),
	.key_i (KEY[1]),
	.key_pressed_stb_o (key1_pressed_stb),
	.key_released_stb_o (),
	.key_state_o ()
);

assign s_rst = key0_pressed_stb | key1_pressed_stb;

always_comb begin
  h_pres = 0;
  m_pres = 0;
  s_pres = 0;
  
  if(key0_pressed_stb) begin
    h_pres = 0;
    m_pres = 0;
    s_pres = 0;
  end else if(key1_pressed_stb) begin
    h_pres = 23;
    m_pres = 59;
    s_pres = 55;
  end
end

hms_clock #(.CLOCK_FREQ(50_000_000)) dut (
	.clk_i (CLOCK_50), 	// input clock for clock
	.srst_i (s_rst),		// reset clock or preset
	
	/* preset values */
	.h_i (h_pres),	// hours
	.m_i (m_pres),	// minutes
	.s_i (s_pres),	// seconds
	.ms_i (10'b0),	// milliseconds
	
	.h (h),	// hours
	.m (m),	// minutes
	.s (s),	// seconds
	.ms ()	// milliseconds
);

div10 #(.N(7)) div_h (.i(h), .o(h_1));
div10 #(.N(7)) div_m (.i(m), .o(m_1));
div10 #(.N(7)) div_s (.i(s), .o(s_1));

always_comb begin
  h_0 = h - (h_1 << 3) - (h_1 << 1);
  m_0 = m - (m_1 << 3) - (m_1 << 1);
  s_0 = s - (s_1 << 3) - (s_1 << 1);
end

SEG7_LUT_6 u0  ( 
  .iDIG  ( {h_1[3:0],h_0[3:0],m_1[3:0],m_0[3:0],s_1[3:0],s_0[3:0]} ),
  .oSEG0 ( HEX0 ),
  .oSEG1 ( HEX1 ),
  .oSEG2 ( HEX2 ),
  .oSEG3 ( HEX3 ),
  .oSEG4 ( HEX4 ),
  .oSEG5 ( HEX5 ),
);

endmodule
