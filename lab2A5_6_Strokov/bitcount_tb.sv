module bitcount_tb;

localparam int N = 16;

logic [N-1:0] inputData;

logic [N-1:0] outputLeft;
logic [N-1:0] outputRight;

logic [$clog2(N):0]  zeros, ones;



initial  begin
  $display ();
  $display (" ========== START TEST (counter) ========== ");
  
  
  
  inputData <= 16'h1;
  #5ns;
  inputData <= 16'h2;
  #5ns;
  inputData <= 16'hA3;
  #5ns;
  inputData <= 16'h0;
  #5ns;
  inputData = 16'hFFFE;
  #5ns;
  inputData = 16'hFFFF;
  #5ns;
  
end

assign ones = N - zeros;

always_comb begin
  zeros = 0;
  for(int i=0;i<N;i++)
    if(inputData[i] == 1'b0)
      zeros = zeros + 1;
end

/* right one */
logic [N-1:0] right_c;
logic [N-1:0] right_a;

assign right_c = inputData | right_a;
assign outputRight = inputData & ~right_a;

always_comb begin
  right_a = 0;  
  right_a[0] = 1'b0;
  for(int i = 1; i < N; i++) begin
    right_a[i] = right_c[i-1];
  end
end

/* left one */
logic [N-1:0] left_c;
logic [N-1:0] left_a;

assign left_c = inputData | left_a;
assign outputLeft = inputData & ~left_a;

always_comb begin
  left_a = 0;  
  left_a[N-1] = 1'b0;
  for(int i = 0; i < N-1; i++) begin
    left_a[i] = left_c[i+1];
  end
end

endmodule;
