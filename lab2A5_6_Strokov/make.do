vlib work


# compiling systemerilog files
vlog *.sv

# name for your testbench module
vsim -t 1ns -msgmode both -novopt bitcount_tb

#adding waveforms in hex view

add wave -hex -radix hexadecimal -label inputData(h) -color blue /bitcount_tb/inputData
add wave -hex -radix binary -label inputData(b) -color blue /bitcount_tb/inputData
add wave -hex -radix binary -label outputLeft -color green /bitcount_tb/outputLeft
add wave -hex -radix binary -label outputRight -color green /bitcount_tb/outputRight
add wave -hex -radix unsigned -color green -label zeros /bitcount_tb/zeros
add wave -hex -radix unsigned -color green -label ones /bitcount_tb/ones
add wave -hex -radix binary -color goldenrod -label "right a" /bitcount_tb/right_a
add wave -hex -radix binary -color goldenrod -label "right c" /bitcount_tb/right_c
add wave -hex -radix binary -color goldenrod -label "left a" /bitcount_tb/left_a
add wave -hex -radix binary -color goldenrod -label "left c" /bitcount_tb/left_c
# running simulation
run 32ns
