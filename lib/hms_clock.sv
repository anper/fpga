/*
\date 2015
\authors Andrew Strokov
\brief Lab 2, task A1: clock
\copyright Metrotek FPGA lessons
\input hours, minutes, seconds, milliseconds to preset value

Clock: hours, minutes, seconds, milliseconds
default clk_i 50 MHz
srst_i to reset value
\input hours, minutes, seconds, milliseconds to preset value
*/

module hms_clock
#(
  CLOCK_FREQ = 5000 // 50_000_000
)
(
	input		clk_i, 		// input clock for clock
	input		srst_i,		// reset clock or preset
	
	/* preset values */
	input[6:0]	h_i,	// hours
	input[6:0]	m_i,	// minutes
	input[6:0]	s_i,	// seconds
	input[9:0]	ms_i,	// milliseconds
	
	output logic [6:0]	h,	// hours
	output logic [6:0]	m,	// minutes
	output logic [6:0]	s,	// seconds
	output logic [9:0]	ms	// milliseconds
);
localparam MS_DIVIDER = CLOCK_FREQ/1000 - 1;

reg h_clk, m_clk, s_clk, ms_clk;
logic [$clog2(MS_DIVIDER):0] cnt;

/* base clock block */
always_ff @(posedge clk_i) begin

	if(srst_i) // preset circuit
		cnt <= 1'b0;
	else
		if(cnt >= MS_DIVIDER)
			cnt <= 1'b0;
		else
			cnt <= cnt + 1'b1;
	
end

/* base up-chain clock signal */
assign ms_clk = (cnt == MS_DIVIDER);

/* milliseconds block */
always_ff @(posedge clk_i) begin

	if(srst_i) // preset circuit
		ms <= ms_i;
	else if(ms_clk) // counter
		if(ms >= 999)
			ms <= 1'b0;
		else
			ms <= ms + 1'b1;
			
end
/* milliseconds up-chain clock signal */
assign s_clk = (ms == 999 && cnt == MS_DIVIDER);

/* seconds block */
always_ff @(posedge clk_i) begin

	if(srst_i) // preset circuit
		s <= s_i;
	else if(s_clk) // counter
		if(s >= 59)
			s <= 1'b0;
		else
			s <= s + 1'b1;
			
end
/* seconds up-chain clock signal */
assign m_clk = (s == 59 && ms == 999 && cnt == MS_DIVIDER);

/* minutes block */
always_ff @(posedge clk_i) begin

	if(srst_i) // preset circuit
		m <= m_i;
	else if(m_clk) // counter
		if(m >= 59)
			m <= 1'b0;
		else
			m <= m + 1'b1;
			
end
/* minutes up-chain clock signal */
assign h_clk = (m == 59 && s == 59 && ms == 999 && cnt == MS_DIVIDER);

/* hour block */
always_ff @(posedge clk_i) begin

	if(srst_i) // preset circuit
		h <= h_i;
	else if(h_clk) // counter
		if(h >= 23)
			h <= 1'b0;
		else
			h <= h + 1'b1;
			
end
/* hours up-chain clock signal */
/*
assign x_clk = (h == 59 && m == 59 && s == 59 && ms == 999 && cnt == MS_DIVIDER);
*/

endmodule
