module div10
#( N = 32)
(
  input [N-1:0] i,
  output logic [N-1:0] o
);

logic [N-1:0] q0, q1, q2, q3;

always_comb begin
  q0 = (i >> 1) + (i >> 2);
  q1 = q0 + (q0 >> 4);
  q2 = q1 + (q1 >> 8);
  q3 = (q2 + (q2 >> 16)) >> 3;
  o = q3 + (((i - q3*10) + 6) >> 4);
end

endmodule