module syncchain
#(SIZE = 2)
(
  input i,
  input clk_i,
  output o
);

logic logic_syncchain [SIZE-1:0];

assign o = logic_syncchain[SIZE-1];

always_ff @(posedge clk_i) begin
  logic_syncchain[0] <= i;
  for(int i=1;i < SIZE; i++)
    logic_syncchain[i] <= logic_syncchain[i-1];
end

endmodule