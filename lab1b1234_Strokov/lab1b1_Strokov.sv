/*
\date 2015
\authors Andrew Strokov
\brief Lab 1, part B, task 1-4: coder, decoder, mux and demux
\copyright Metrotek FPGA lessons

Translate position OUT_SIZE input into spme OUT_SIZE binary code
*/

module lab1b1_Strokov
#(
  parameter IN_SIZE   = 16,
  parameter OUT_SIZE	 = 4
)
(
	input		[IN_SIZE-1:0]	in, // decoder n-position input
	input		muxed,				 // demultiplexer data input
	
	output	[OUT_SIZE-1:0]	out,		// binary coder output
	output	[IN_SIZE-1:0] 	decoded, // n-position decoder output for binary code
	output	[IN_SIZE-1:0] 	demuxed, // demux signal from demultplexer with same binary code
	output	remuxed						// multiplxer data output, must be the same as 'muxed'
);

/* TODO: add assert for IN_SIZE > 2^OUT_SIZE */

/* coder block */
always_comb begin

	out = 0;
	for (int i =0; i < IN_SIZE; i++) begin
		if(in[i]) begin
			out = i[OUT_SIZE-1:0];
		end 
	end
	
end

/* decoder */
always_comb begin
	decoded = 0;
	decoded[out] = 1'b1;
end

/*  demux */
always_comb begin
	demuxed = 0;
	demuxed[out] = muxed;
end

/* mux block */
always_comb begin
	remuxed = demuxed[out];
end

endmodule
