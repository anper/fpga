/*
\date 2015
\authors Andrew Strokov
\brief Lab 1, part B, task 5: reversive counter
\copyright Metrotek FPGA lessons


*/

module lab1b5_Strokov
#(
  parameter SIZE	= 4,
  parameter	INC	= 1
)
(
	input		clk, 		// count on positive edge
	input		reverse,	// reverse counter direction
	
	output	[SIZE-1:0]	x
);

always_ff @(posedge clk) begin

	if(!reverse) begin
		x <= x + INC[SIZE-1:0];
	end else begin
		x <= x - INC[SIZE-1:0];
	end
	
end


endmodule
