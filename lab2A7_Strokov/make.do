vlib work


# compiling systemerilog files
vlog *.sv
vlog ../lib/*.sv

# name for your testbench module
vsim -t 1ns -msgmode both -novopt debounce_tb

#adding waveforms in hex view

add wave -hex -radix hexadecimal -label button -color blue /debounce_tb/key
add wave -hex -radix binary -label clk -color blue /debounce_tb/clk

add wave -hex -radix binary -label press -color green /debounce_tb/dut/key_pressed_stb_o
add wave -hex -radix binary -label release -color green /debounce_tb/dut/key_released_stb_o
add wave -hex -radix binary -label state -color green /debounce_tb/dut/key_state_o

add wave -hex -radix unsigned -label counter -color goldenrod  /debounce_tb/dut/cnt
add wave -hex -radix binary -label key_pressed -color goldenrod /debounce_tb/dut/key_pressed
add wave -hex -radix binary -label key_released -color goldenrod /debounce_tb/dut/key_released
add wave -hex -radix binary -label syncchain -color goldenrod /debounce_tb/dut/key_chain/logic_syncchain

# running simulation
run 80us
