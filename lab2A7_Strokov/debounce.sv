/*
\date 2015
\authors Andrew Strokov
\brief Lab 2, task A7: debounce module
\copyright Metrotek FPGA lessons


*/

module debounce
#(
  CLOCK_FREQ = 1000, //50_000_000
  DEBOUNCE_TIME = 15 // in us
)
(
	input clk_i, // input clock

	input key_i, // ~button press
	
	output logic key_pressed_stb_o = 1'b0, // one-shot key pressed
	output logic key_released_stb_o = 1'b0, // one-shot key pressed
	output logic key_state_o = 1'b0 // one-shot key pressed
);

localparam COUNT = CLOCK_FREQ * DEBOUNCE_TIME/1_000_000;

logic [$clog2(COUNT)-1:0] cnt = 0;

logic key;
syncchain #(.SIZE(2)) key_chain (
  .clk_i(clk_i),
  .i(~key_i),
  .o(key)
);

logic key_pressed;
assign key_pressed = (cnt == 0) & key & ~key_state_o;

logic key_released;
assign key_released = (cnt == 0) & ~key & key_state_o;

always_ff @(posedge clk_i)
  if(key_pressed_stb_o)
    key_pressed_stb_o <= 1'b0;
  else
    key_pressed_stb_o <= key_pressed;

always_ff @(posedge clk_i)
  if(key_released_stb_o)
    key_released_stb_o <= 1'b0;
  else
    key_released_stb_o <= key_released;

always_ff @(posedge clk_i)
  if(key_pressed)
    key_state_o <= 1'b1;
  else if (key_released)
    key_state_o <= 1'b0;

always_ff @(posedge clk_i)
  if(cnt > 0)
    cnt <= cnt - 1;
  else if(key_released | key_pressed)
    cnt <= COUNT;

endmodule