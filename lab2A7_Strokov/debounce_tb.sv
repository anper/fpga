module debounce_tb;

logic clk;
initial begin
    clk = 1'b0;
    forever begin
        #500ns; // 1 MHz
        clk = !clk;
    end
end

logic key;

initial  begin
  key <= 1'b1;
  #20us;
  
  key <= 1'b0;
  #2500ns;
  key <= 1'b1;
  #1900ns;
  key <= 1'b0;
  #1200ns;
  key <= 1'b1;
  #3500ns;
  key <= 1'b0;
  #5500ns;
  key <= 1'b1;
  #1900ns;
  key <= 1'b0;
  #1200ns;
  key <= 1'b1;
  
  #20us;
  
  key <= 1'b0;
  #500ns;
  key <= 1'b1;
  #1200ns;
  key <= 1'b0;
  #1200ns;
  key <= 1'b1;
  #900ns;
  key <= 1'b0;
  #1600ns;
  key <= 1'b1;
  
end

debounce #(.CLOCK_FREQ(1_000_000), .DEBOUNCE_TIME(10)) dut (
	.clk_i(clk),
	.key_i (key),
	.key_pressed_stb_o (),
	.key_released_stb_o (),
	.key_state_o ()
);

endmodule;
	