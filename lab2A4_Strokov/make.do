vlib work


# compiling systemerilog files
vlog *.sv

# name for your testbench module
vsim -t 1ns -msgmode both -novopt trafficLight_tb

#adding waveforms in hex view

add wave -hex  -color blue -label clk /trafficLight_tb/clk
add wave -hex  -color blue -label rst /trafficLight_tb/rst 

add wave -hex  -color red -label red /trafficLight_tb/dut/red_o 
add wave -hex  -color yellow -label yellow /trafficLight_tb/dut/yellow_o 
add wave -hex  -color green -label green /trafficLight_tb/dut/green_o 

#TL internal
add wave -hex -color goldenrod -label state /trafficLight_tb/dut/state 
add wave -hex -color goldenrod -label next /trafficLight_tb/dut/next_state 
add wave -hex -color goldenrod -radix unsigned -label wait_cnt /trafficLight_tb/dut/wait_cnt 
add wave -hex -color goldenrod -radix unsigned -label blink_cnt /trafficLight_tb/dut/blink_cnt
add wave -hex -color goldenrod -label blink /trafficLight_tb/dut/blink

# running simulation
run 300us
