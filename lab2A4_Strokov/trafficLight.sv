/*
\date 2015
\authors Andrew Strokov
\brief Lab 2, task A4: traffic Light
\copyright Metrotek FPGA lessons

phases:
red, yellow_red, green, green_blink, yellow
*/



module trafficLight
#(
	CLOCK = 5000, // 50_000_000
	BLINKTIME = 2, // in ms
	REDTIME = 1, // in ms
	YELLOWTIME = 2,
	GREENBLINKTIME = 4,
	GREENTIME =  10 // in ms
	
)
(
	input	clk_i,  // input clock
	input	rst_i,	// async reset

	output	red_o,
	output	yellow_o,
	output	green_o
);

localparam BLINKDIV = BLINKTIME * CLOCK / 1000;
localparam WAITDIV = GREENTIME * CLOCK / 1000; //TODO $max($max(REDTIME,GREENTIME),WAITTIME) * CLOCK / 1000;

enum logic [2:0] {red_s, yellow_red_s, green_s, green_blink_s, yellow_s} state, next_state;

logic [$clog2(WAITDIV):0] b [4:0];
assign b[red_s] = REDTIME * CLOCK / 1000;
assign b[yellow_red_s] = YELLOWTIME * CLOCK / 1000;
assign b[green_s] = GREENTIME * CLOCK / 1000;
assign b[green_blink_s] = GREENBLINKTIME * CLOCK / 1000;
assign b[yellow_s] = YELLOWTIME * CLOCK / 1000;


reg blink;
reg [$clog2(BLINKDIV):0] blink_cnt;

reg [$clog2(WAITDIV):0] wait_cnt;

// blink counter and blink generator
always_ff @(posedge clk_i or posedge rst_i) begin
  if(rst_i)
    blink_cnt <= 0;
  else
    if(blink_cnt < BLINKDIV-1 && state == green_blink_s)
      blink_cnt <= blink_cnt + 1;
    else
      blink_cnt <= 0;
end

assign blink = (blink_cnt < BLINKDIV/2);

/* light logic */
assign red_o = ((state == red_s) || (state == yellow_red_s)); // && clk_i;
assign yellow_o = ((state == yellow_red_s) || (state == yellow_s)); // && clk_i;
assign green_o = ((state == green_s) || (state == green_blink_s && blink)); // && clk_i;


/* wait counter */
always_ff @(posedge clk_i or posedge rst_i)
  if(rst_i)
    wait_cnt <= REDTIME * CLOCK / 1000 - 1;
  else if(wait_cnt > 0)
    wait_cnt <= wait_cnt - 1;
  else
    wait_cnt <= b[next_state] - 1;
    
/* FSM */
always_ff @(posedge clk_i or posedge rst_i)
  if(rst_i)
    state <= red_s;
  else
    state <= next_state;

always_comb begin
  next_state = state;
  
    case (state)
      red_s: begin
	if(wait_cnt == 0)
	  next_state = yellow_red_s;
      end

      yellow_red_s: begin
	if(wait_cnt == 0)
	  next_state = green_s;
      end
	
      green_s: begin
	if(wait_cnt == 0)
	  next_state = green_blink_s;
      end
	
      green_blink_s: begin
	if(wait_cnt == 0)
	  next_state = yellow_s;
      end
	
      yellow_s: begin
	if(wait_cnt == 0)
	  next_state = red_s;
      end
	
      default:
	next_state = red_s;
    endcase
    
end

endmodule
