module trafficLight_tb;

logic clk;

/* clock oscillator */
initial begin
    clk = 1'b0;
    forever begin
        #500ns; // 1 MHz
        clk = !clk;
    end
end

logic rst;



initial  begin
  rst <= 1'b0;
  @( posedge clk );
  rst <= 1'b1;
  @( negedge clk );
  rst <= 1'b0;
  
  $display ();
  $display (" ========== START TEST (serialiser) ========== ");
   
end

trafficLight #(.CLOCK(1000)) dut  (
	.clk_i (clk), 		// input clock
	.rst_i (rst),		// async reset
	
	/* NC */
	.red_o(),
	.yellow_o(),
	.green_o()

);



endmodule;
