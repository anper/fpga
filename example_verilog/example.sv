module example
#(
  parameter MAX_VALUE   = 15,
  parameter MATCH_VALUE = 7
)
(
  input        clk_i,
  input        rst_i,

  output [7:0] cnt_o,
  output       match_o

);


always_ff @( posedge clk_i or posedge rst_i )
  if( rst_i )
    cnt_o <= '0;
  else
    if( cnt_o >= MAX_VALUE )
      cnt_o <= '0;
    else
      cnt_o <= cnt_o + 1'd1;

assign match_o = ( cnt_o == MATCH_VALUE );

endmodule
