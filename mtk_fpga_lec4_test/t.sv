module t(
  input       clk_i,
  input       rst_i,

  input [3:0] data_i,
  input       data_load_i
  
  // �������, ��� ��� ��������� ������� 
  // �������� ������

);

logic [3:0] a;
logic [3:0] b;
logic [3:0] b_d1;
logic [3:0] c;
logic [3:0] d;

logic       m;
logic       k;

always_ff @( posedge clk_i or posedge rst_i )
  if( rst_i )
    a <= 'd5;
  else
    if( data_load_i )
      a <= data_i - 'd2;
    else
      a <= a + 1'd1;

always_ff @( posedge clk_i or posedge rst_i )
  if( rst_i )
    begin
      b <= '0;
      c <= '0;
    end
  else
    begin
      b <= a;
      c <= b;
    end

always_ff @( posedge clk_i or posedge rst_i )
  if( rst_i )
    b_d1 <= '0;
  else
    b_d1 <= b;

assign m = ( b_d1 > a );

always_comb
  begin
    k = 1'b1;

    if( m && ( c > 7 ) )
      k = 1'b0;
  end

always_ff @( posedge clk_i )
  if( !k )
    d <= a;
  else
    if( |d[1:0] )
      d <= { d[2:0], d[3] };

endmodule
