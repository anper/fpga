module t_tb;

logic clk;

initial
  begin
    clk = 1'b0;
    forever
      begin
        #5ns;
        clk = !clk;
      end
  end

logic rst;

logic  [3:0] data;
logic        data_load;

initial
  begin
    rst <= 1'b1;
    @( negedge clk );
    rst <= 1'b0;
  end

// DUT/UUT
t t(

  .clk_i                                  ( clk               ),
  .rst_i                                  ( rst               ),
 
  .data_i                                 ( data              ),
  .data_load_i                            ( data_load         )

);

initial
  begin
    data      = '0;
    data_load = '0;

    repeat (4) @( posedge clk );
    data      <= 'd4;
    data_load <= 1'b1;
    
    @( posedge clk );
    data      <= 'd0;
    data_load <= 1'b0;
  end



endmodule
