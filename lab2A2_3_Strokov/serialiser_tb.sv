module serialiser_tb;

logic clk;

/* clock oscillator */
initial begin
    clk = 1'b0;
    forever begin
        #500ns; // 1 MHz
        clk = !clk;
    end
end

logic rst;

// output signal
logic testData;
logic outputData;
logic outputCTS;
logic outputBusy;

// input data values
logic	[15:0]	inputData; // input data []
logic	[4:0]	inputDataSize; // valid data size [
logic	inputDataRTS; // one-shot datavalid signal

task sendMessage(int data, int size, int holdoff);
  wait(outputBusy == 0);
  
  inputData <= data & (2**size - 1);
  inputDataSize <= size;
  inputDataRTS <= 1'b1;
  repeat (1) @( posedge clk );
  inputDataRTS <= 1'b0;
  repeat (holdoff) @( posedge clk );
endtask

/* input data source */
int data;
int dataSize;

initial  begin
  rst <= 1'b0;
  repeat (1) @( posedge clk );
  rst <= 1'b1;
  testData <= 1'b0;
  
  repeat (1) @( posedge clk );
  rst <= 1'b0;
  
  $display ();
  $display (" ========== START TEST (serialiser) ========== ");
  
  sendMessage(16'hDEAD, 5'd16, 1);
  sendMessage(16'hBEEF, 5'd16, 1);
  sendMessage(16'h42, 5'd2, 1);
  sendMessage(16'h42, 5'd8, 1);
  sendMessage(16'hFF, 5'd3, 1);
  
end

/* verification */
int ok, shortMessage;

initial begin
  forever begin
    @ ( posedge inputDataRTS ); //  start verification
    
    /* // show data
    $display ("data: %x (%b)", inputData, inputData);
    
    for(int i = 0; i < inputDataSize; i++) 
      $display("test: data[%d] = %d", i, inputData[i]);
    for(int i = 1; i <= inputDataSize; i++) 
      $display("inv test: for %d - data[%d] = %d", i, inputDataSize-i, inputData[inputDataSize-i]);
    */
    
    ok = 1;
    shortMessage = 0;
    
    for(int i = 1; i <= inputDataSize; i++) begin
      @( posedge clk );
      testData <= inputData[inputDataSize-i]; // only for wave visualiser
      #100ns; // async cheat
      
      /* if serialiser not response -- exit */
      if(!outputCTS) begin
	shortMessage = 1;
	break;
      end
      
      /* else check data */
      if(inputData[inputDataSize-i] != outputData) begin
	$warning("%m: no match at %d bit (out - %b, test - %b)", inputDataSize-i, outputData, inputData[inputDataSize-i]);
	ok = 0;
      end
      
    end
    
    @( posedge clk );
    testData <= 1'b0;
    
    /* varification logic */
    if(shortMessage)
      if(inputDataSize < 3)
	$info("word[%d] 0x%X too small, passed", inputDataSize, inputData);
      else
	$error("word[%d] 0x%X invalid, failed", inputDataSize, inputData);
    else if(ok)
      if(inputDataSize < 3)
	$error("word[%d] 0x%X validated too small, failed", inputDataSize, inputData);
      else
	$info("word[%d] 0x%X passed", inputDataSize, inputData);
    else
      $error("word[%d] 0x%X unmached data, failed", inputDataSize, inputData);
      
  end
end


serialiser dut_ser (
	.clk_i (clk), 		// input clock
	.rst_i (rst),		// async reset
	
	.data_i (inputData),		// input data [DATASIZE-1:0]
	.data_mod_i (inputDataSize), 	// valid data size [MODSIZE-1:0]
	.data_val_i (inputDataRTS),	// one-shot datavalid signal
	.ser_data_o (outputData),
	.ser_data_val_o(outputCTS),
	.busy_o(outputBusy),
	
	/* NC */
	.cnt_o()
);

deserialiser dut_deser (
	.clk_i (clk), 		// input clock
	.rst_i (rst),		// async reset
	
	
	.ser_data_i(outputData),	// input serialised data
	.ser_data_val_i(outputCTS),	// high level for receive
	
	.data_o(),	// output data [DATASIZE-1:0] 
	.data_mod_o(), 	// valid data size [MODSIZE-1:0]
	.data_val_i()	// one-shot datavalid signal
);


endmodule;
