/*
\date 2015
\authors Andrew Strokov
\brief Lab 2, task A3: deserialiser
\copyright Metrotek FPGA lessons


*/

module deserialiser
#(
	DATASIZE = 16,
	MODSIZE  = 5,
	VALIDMIN = 3
)
(
	input	clk_i,	// input clock
	input	rst_i,	// async reset
	
	input	ser_data_i,	// input serialised data
	input	ser_data_val_i,	// high level for receive
	
	output	logic [DATASIZE-1:0] data_o,	 // output data
	output	logic [MODSIZE-1:0]  data_mod_o, // valid data size
	output 	logic		     data_val_i // one-shot datavalid signal
	
);

/* TODO: assert for MODSIZE > $clog2(DATASIZE) */

reg [DATASIZE-1:0] data_reg;
reg [MODSIZE-1:0]  cnt;

logic [DATASIZE-1:0] data_reg_LE;


always_comb begin
  data_reg_LE = 0;
  for(int i=0;i < cnt; i++)
    if(i < DATASIZE)
      data_reg_LE[i] = data_reg[DATASIZE-(cnt)+i];
end

always_ff @(posedge clk_i or posedge rst_i) begin
  if(rst_i)
    data_reg <= 0;
  else if(ser_data_val_i)
    data_reg[DATASIZE-1-cnt] <= ser_data_i;
  else
    data_reg <= 0;
end

always_ff @(posedge clk_i or posedge rst_i) begin
  if(rst_i) begin
    data_o <= 0;
    data_mod_o <= 0;
    
    data_val_i <= 0;
  end else if(cnt >= VALIDMIN && !ser_data_val_i) begin
    data_o <= data_reg_LE;
    data_mod_o <= cnt;
    data_val_i <= 1'b1;
  end else
    data_val_i <= 0;
end

always_ff @(posedge clk_i or posedge rst_i) begin
  if(rst_i)
    cnt <= 0;
  else if(!ser_data_val_i)
    cnt <= 0;
  else
    cnt <= cnt + 1;
end


endmodule
