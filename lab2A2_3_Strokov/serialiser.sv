/*
\date 2015
\authors Andrew Strokov
\brief Lab 2, task A2: serialiser
\copyright Metrotek FPGA lessons


*/

module serialiser
#(
	DATASIZE = 16,
	MODSIZE  = 5,
	VALIDMIN = 3
)
(
	input						clk_i, 		// input clock
	input						rst_i,		// async reset
	
	input	[DATASIZE-1:0]	data_i,		// input data
	input	[MODSIZE-1:0]	data_mod_i, // valid data size
	input 					data_val_i,	// one-shot datavalid signal
	

	output	logic ser_data_o,		// output serialised data
	output	logic ser_data_val_o,//	high level durning transmit
	output	logic busy_o,			//	high level durning transmit
	output [MODSIZE-1:0] cnt_o
);

/* TODO: assert for MODSIZE > $clog2(DATASIZE) */

reg [DATASIZE-1:0]	data_reg;
reg [MODSIZE-1:0]	cnt;
logic data_val_load; // valid data

assign busy_o = ser_data_val_o;
assign data_val_load = data_val_i && (data_mod_i >= VALIDMIN) && !busy_o;

assign cnt_o = cnt;

// serialiser state
always_ff @(posedge clk_i or posedge rst_i) begin
	if(rst_i)
		ser_data_val_o <= 0;
	else if (data_val_load)
		ser_data_val_o <= 1;
	else if (cnt == 0)
		ser_data_val_o <= 0;
end

// load data -> internal register
always_ff @(posedge clk_i or posedge rst_i) begin
	if(rst_i)
		data_reg <= 0;
	else if (data_val_load)
		data_reg <= data_i;
end
	
// counter
always_ff @(posedge clk_i or posedge rst_i) begin
	if(rst_i)
		cnt <= 0;
	else if(data_val_load)
		cnt <= data_mod_i - 1;
	else if(cnt > 0)
		cnt <= cnt - 1;		
end

// data output
always_ff @(posedge clk_i or posedge rst_i) begin
	if(rst_i)
		ser_data_o <= 0;
	else if(cnt > 0)
		ser_data_o <= data_reg[cnt-1];
	else if (data_val_load)
		ser_data_o <= data_i[data_mod_i-1];
	else
		ser_data_o <= 0;
end

endmodule
