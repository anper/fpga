vlib work


# compiling systemerilog files
vlog *.sv

# name for your testbench module
vsim -t 1ns -msgmode both -novopt serialiser_tb

#adding waveforms in hex view

add wave -hex  -color blue /serialiser_tb/clk
add wave -hex  -color blue /serialiser_tb/rst

add wave -hex -color blue -radix hexadecimal /serialiser_tb/inputData
add wave -hex -color blue -radix unsigned 	/serialiser_tb/inputDataSize
add wave -hex -color blue /serialiser_tb/inputDataRTS

add wave -hex -color red /serialiser_tb/dut_ser/ser_data_val_o

add wave -hex -color red /serialiser_tb/outputData
add wave -hex -color green /serialiser_tb/testData

#serialiser internal
add wave -hex -color yellow -radix unsigned /serialiser_tb/dut_ser/cnt_o
add wave -hex -color yellow -radix binary /serialiser_tb/dut_ser/data_reg

#deserialiser internal
#add wave -hex -color goldenrod -radix binary /serialiser_tb/dut_deser/data_o
add wave -hex -color goldenrod -radix hexadecimal /serialiser_tb/dut_deser/data_o
add wave -hex -color goldenrod -radix unsigned /serialiser_tb/dut_deser/data_mod_o
add wave -hex -color goldenrod -radix unsigned /serialiser_tb/dut_deser/data_val_i
#add wave -hex -color goldenrod -radix unsigned /serialiser_tb/dut_deser/cnt
#add wave -hex -color goldenrod -radix binary /serialiser_tb/dut_deser/data_reg
#add wave -hex -color goldenrod -radix binary /serialiser_tb/dut_deser/data_reg_LE

# running simulation for 1000 nanoseconds
# you can change for run -all for infinity simulation :-)
run 55us
