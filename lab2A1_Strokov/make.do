vlib work


# compiling systemerilog files
vlog *.sv 
vlog ../lib/*.sv

# name for your testbench module
vsim -t 1ns -novopt hms_clock_tb

#adding all waveforms in hex view
add wave -hex -radix binary -label clock -color blue /hms_clock_tb/clk
add wave -hex -radix binary -label reset -color blue /hms_clock_tb/rst

add wave -hex -radix unsigned -label h_pres -color blue /hms_clock_tb/h_pres
add wave -hex -radix unsigned -label m_pres -color blue /hms_clock_tb/m_pres
add wave -hex -radix unsigned -label s_pres -color blue /hms_clock_tb/s_pres

add wave -hex -radix unsigned -label h   -color green /hms_clock_tb/h
add wave -hex -radix hexadecimal -label h_1 -color yellow /hms_clock_tb/h_1
add wave -hex -radix hexadecimal -label h_0 -color yellow /hms_clock_tb/h_0

add wave -hex -radix unsigned -label reset -color green /hms_clock_tb/m
add wave -hex -radix hexadecimal -label m_1 -color yellow /hms_clock_tb/m_1
add wave -hex -radix hexadecimal -label m_0 -color yellow /hms_clock_tb/m_0

add wave -hex -radix unsigned -label s -color green /hms_clock_tb/s
add wave -hex -radix hexadecimal -label s_1 -color yellow /hms_clock_tb/s_1
add wave -hex -radix hexadecimal -label s_0 -color yellow /hms_clock_tb/s_0

add wave -hex -radix hexadecimal -label s_0 -color "Lime Green" /hms_clock_tb/test
# add wave -r -hex *


# running simulation for 1000 nanoseconds
# you can change for run -all for infinity simulation :-)
run 45ms
