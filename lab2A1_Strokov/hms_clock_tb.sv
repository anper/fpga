module hms_clock_tb;

logic clk;

initial begin
    clk = 1'b0;
    forever begin
        #500ns; // 1 MHz
        clk = !clk;
    end
end

logic rst;

// output signal
logic	[6:0]	h, h_0, h_1;	// hours
logic	[6:0]	m, m_0, m_1;	// minutes
logic	[6:0]	s, s_0, s_1;	// seconds

// input preset values
logic	[6:0]	h_pres;	// hours
logic	[6:0]	m_pres;	// minutes
logic	[6:0]	s_pres;	// seconds

initial  begin
  rst <= 1'b0;
  repeat (4) @( negedge clk );
  
  h_pres <= 12;
  m_pres <= 32;
  s_pres <= 28;
  
  rst <= 1'b1;
  @( negedge clk );
  rst <= 1'b0;
  h_pres <= 0;
  m_pres <= 0;
  s_pres <= 0;
  
  repeat (9*1000) @( negedge clk ); // run 9 ms
  
  
  rst <= 1'b1;
  h_pres <= 12;
  m_pres <= 32;
  s_pres <= 28;
  @( posedge clk );
  rst <= 1'b0;
  h_pres <= 0;
  m_pres <= 0;
  s_pres <= 0;
  
  repeat (9*1000) @( negedge clk ); // run 9 ms
  
  h_pres <= 12;
  m_pres <= 32;
  s_pres <= 59;
  
  rst <= 1'b1;
  @( negedge clk );
  rst <= 1'b0;
  h_pres <= 0;
  m_pres <= 0;
  s_pres <= 0;
  
  repeat (9*1000) @( negedge clk ); // run 9 ms
  
  h_pres <= 12;
  m_pres <= 59;
  s_pres <= 59;
  
  rst <= 1'b1;
  @( negedge clk );
  rst <= 1'b0;
  h_pres <= 0;
  m_pres <= 0;
  s_pres <= 0;
  
  repeat (9*1000) @( negedge clk ); // run 9 ms
  
  h_pres = 23;
  m_pres = 59;
  s_pres = 59;
  
  rst <= 1'b1;
  @( negedge clk );
  rst <= 1'b0;
  h_pres <= 0;
  m_pres <= 0;
  s_pres <= 0;
  
end

hms_clock #(.CLOCK_FREQ(1_000)) dut (
	.clk_i (clk), 		// input clock for clock
	.srst_i (rst),		// reset clock or preset
	
	/* preset values */
	.h_i (h_pres),	// hours
	.m_i (m_pres),	// minutes
	.s_i (s_pres),	// seconds
	.ms_i (10'b0),	// milliseconds
	
	.h (h),	// hours
	.m (m),	// minutes
	.s (s),	// seconds
	.ms ()	// milliseconds
);

div10 #(.N(7)) div_h (.i(),.o());
assign div_h.i = h;
assign h_1 = div_h.o;

div10 #(.N(7)) div_m (.i(m), .o(m_1));
div10 #(.N(7)) div_s (.i(s), .o(s_1));

always_comb begin
  h_0 = h - (h_1 << 3) - (h_1 << 1);
  m_0 = m - (m_1 << 3) - (m_1 << 1);
  s_0 = s - (s_1 << 3) - (s_1 << 1);
end

logic [23:0] test;
assign test = {h_1[3:0],h_0[3:0],m_1[3:0],m_0[3:0],s_1[3:0],s_0[3:0]};

endmodule;
